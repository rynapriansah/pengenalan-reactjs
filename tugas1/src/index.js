import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import App from './App';
// import Fungsi from './components/Fungsi';
// import Form from './components/Form/Form';
import * as serviceWorker from './serviceWorker';

const MainHeader = React.createElement(
  'h2',
  {
    id: 'main-header',
    title: 'ini adalah title untuk main header'
  },
  'Header utama'
);

const imgSrc = 'https://drive.google.com/uc?export=view&id=1egLzv1DMR4-jiv85Ncpxw-sogCexIfx6'
const imgWidth = '500px'
const imgHeight = '350px'

// contoh elemen dengan child: img di dalam p
// contoh ini juga menggunakan ekspresi
const MainImg = (
  <p>
    <img src={imgSrc} width={imgWidth} height={imgHeight}/>
  </p>
)


ReactDOM.render(
  MainHeader,
  document.getElementById('root')
);


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
