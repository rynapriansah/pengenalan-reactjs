import React, {Component} from 'react';
import logo from '../../logo.svg';

class Form extends Component {
	render(){
		return(
	<div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />    
        <h3 className="App-link"> Selamat Datang di aplikasi React Js </h3>
        <br/>
      <form>
  <div className="form-group">
    <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email"/>
  </div>
  <div className="form-group">
    <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Password"/>
  </div>
  
  <button type="submit" className="btn btn-dark">Submit</button>
</form>
      </header>

    </div>

		)
	}
}

export default Form;